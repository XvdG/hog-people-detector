#include <ros/ros.h>

#include <image_transport/image_transport.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <vision_msgs/Detection2DArray.h>
#include <vision_msgs/Detection3DArray.h>



using namespace cv;
using namespace std;
bool debug = false;
bool extrawindow = false;


class Detector // Detector class adapted from https://github.com/opencv/opencv/blob/master/samples/cpp/peopledetect.cpp see https://opencv.org/license.html
{
   
    	HOGDescriptor hog;
	public:
    
    	Detector() : hog()
	{
        	hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());
        
    	}
    
    	vector<Rect> detect(InputArray img)
    	{
        	// Run the detector
        	vector<Rect> found;
        
        	hog.detectMultiScale(img, found, 0, Size(8,8), Size(32,32), 1.05, 2, false);
        
        	return found;
    	}
    
	void adjustRect(Rect & r) const
    	{
        	// The HOG detector returns slightly larger rectangles than the real objects,
        	// so we slightly shrink the rectangles to get a nicer output.
        	r.x += cvRound(r.width*0.1);
        	r.width = cvRound(r.width*0.8);
        	r.y += cvRound(r.height*0.07);
        	r.height = cvRound(r.height*0.8);
    }
};



class opencv_solution
{
	public:
				
		ros::NodeHandle nh;
		image_transport::ImageTransport it;
		image_transport::Publisher pub1;
		ros::Publisher pub2;
		image_transport::Subscriber sub;
		
		
		opencv_solution() //Constructor initializes the subscriber and publisher
			: it(nh)
		{	
			if(extrawindow){			
			cv::namedWindow("view");
			cv::startWindowThread();
			}

			pub1 = it.advertise("opencv_solution_node/visual", 1);
			pub2 = nh.advertise<vision_msgs::Detection2DArray>("opencv_solution_node/detections", 1);
			sub=it.subscribe("camera/rgb/image_raw", 1, &opencv_solution::imageCallback, this);
		}
		
		void imageCallback(const sensor_msgs::ImageConstPtr& msg)
		{
			try
  			{
				//Convert ROS image to CV image
				cv_bridge::CvImagePtr cv_ptr =  cv_bridge::toCvCopy(msg,"bgr8");
				//Initialize detector
				Detector detector;
				// Create vector of found persons.
				vector<Rect> found = detector.detect(cv_ptr->image);
	
				// Iterate over vector to draw rectangles around detected persons.
				
				vector<vision_msgs::Detection2D> detections1(found.size());
				

				int count=0;
        			for (vector<Rect>::iterator i = found.begin(); i != found.end(); ++i)
        			{            				
					Rect &r = *i;
            				detector.adjustRect(r);
            				rectangle(cv_ptr->image, r.tl(), r.br(), cv::Scalar(0, 255, 0), 2);
					// vector<Rect> to vector<Detection2D>					
					detections1[count].bbox.size_x = r.width;
					detections1[count].bbox.size_y = r.height;
					detections1[count].bbox.center.x = r.x+0.5*r.width;
					detections1[count].bbox.center.y = r.y+0.5*r.height;
					circle(cv_ptr->image,cv::Point(detections1[count].bbox.center.x , detections1[count].bbox.center.y),5,cv::Scalar(0, 0, 255),2);
	

					// Checking for debug
					if(debug){
						if(r.contains(cv::Point(detections1[count].bbox.center.x , detections1[count].bbox.center.y))){
						ROS_INFO("Conversion to Detection2D OK");
						}
						else{
						ROS_INFO("Conversion to Detection2D Failed");
						ROS_INFO("Center as in D2DA: (%f, %f)", detections1[count].bbox.center.x, detections1[count].bbox.center.y);
						}
					}	
					count++;
					
        			}
				//vector<Detection2D> to Detection2DArray
				vision_msgs::Detection2DArray detectionarray;
				detectionarray.detections = detections1;
		

				//Show image/publish image
				if(extrawindow){
		    		cv::imshow("view",cv_ptr->image);
				}
				
				//Publish visual				
				pub1.publish(cv_ptr->toImageMsg());
				
				//Publish Detection2DArray
				pub2.publish(detectionarray);
				
				
				//Allow eventprocessing by HighGUI
				if(extrawindow){    				
				cv::waitKey(5);}
			}
			catch (cv_bridge::Exception& e)	
  			{
    				ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  			}
  		}
		
	private:

		
};




int main(int argc, char **argv)
{
	//Initialize Ros
  	ros::init(argc, argv, "image_listener");
	// create object from opencv_solution class	
	opencv_solution opencv_solution_object;

	// Let ROS loop  	
	ros::spin();
	// On exit also destroy view window
  	if(extrawindow){
	cv::destroyWindow("view");}
}

